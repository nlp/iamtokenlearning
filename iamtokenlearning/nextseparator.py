#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Learning of the next separator. This corresponds to predicting whether a masked character will be in a given set or not. One usually takes all the re.compile('[\W_]') to be a separator, while the rest are not separator.

This may serve as a restructuration of the tokens from a cleaned text.
"""

import re
from collections import Counter
from datetime import datetime as dt

import logging
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s|%(levelname)s|%(message)s',
                    datefmt="%Y-%m-%d|%H:%M:%S",
                    filename=f"data/nextseparator.log",
                    )

from scipy import sparse as sp
import numpy as np
from iamtokenizing import RegexDetector
from iambagging import BagOfWords

from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.naive_bayes import BernoulliNB, ComplementNB, MultinomialNB
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.neural_network import MLPClassifier
import sklearn.metrics as skmet


filename = "texts/hugo_NotreDameDeParis1.txt"
with open(filename, 'r') as f:
    text_train = f.read()
filename = "texts/hugo_NotreDameDeParis2.txt"
with open(filename, 'r') as f:
    text_test = f.read()

char_count = Counter(text_train + text_test)
_regex = re.compile('[\W_]')
_is_separator = {k: bool(_regex.match(k)) for k in char_count.keys()}
# _is_separator['_'] = True
def is_separator(text):
    return _is_separator[text[0]]


def get_tokens_positions(text):
    """Get all positions from the initial text that corresponds to clean text."""
    # capture the illustration, in the form of '[Illustration blablabla]'
    regex_parentheses = '\[[^\]]+\]'
    tokens_parentheses = RegexDetector(text)
    tokens_parentheses.tokenize(regex_parentheses)
    # capture the fields specific to Gutenberg, in the form of '*** blablabla ***'
    regex_gutenberg = '(\*\*\*)[^\*]+(\*\*\*)'
    tokens_gutenberg = RegexDetector(text)
    tokens_gutenberg.tokenize(regex_gutenberg)
    # withdraw everything before the first / after the last GGutenberg fields
    text_clean = RegexDetector(text, intervals={tokens_gutenberg[0].stop,
                                                tokens_gutenberg[-1].start})
    # withdraw the illustration parenthesis
    text_clean -= tokens_parentheses
    # construct the list of all remaining positions
    positions = list(pos for start, stop in text_clean.intervals
                     for pos in range(start, stop))
    return positions


def construct_tokens_separators(
        text: str,
        ngram_size_left: int = 5,
        ngram_size_right: int = 0):
    """Construct the tokens and the separators from the text.

    Construct two lists:
         - one corresponds to the tokens, in the form of NGrams with size ngram_size_left on the left to ngram_size_right on the right, from a given position that is read from the tokens positions (see this method above). The character at the given position is not present in the extracted NGram.
         - the second list corresponds to whether the character at the given position is a separator (True) or not (False in the list).
    """
    tokens_positions = get_tokens_positions(text)
    tokens, separators = list(), list()
    for pos in range(ngram_size_left,
                     len(tokens_positions) - ngram_size_right - 1):
        positions = tokens_positions[pos - ngram_size_left: pos]
        positions += tokens_positions[pos + 1: pos + 1 + ngram_size_right]
        token = ''.join(text[p] for p in positions)
        separator = is_separator(text[tokens_positions[pos]])
        tokens.append([*token])
        separators.append(separator)
    return tokens, separators


def construct_Xy(*texts, ngram_size_left: int=5, ngram_size_right: int=0):
    bows, ys = list(), list()
    for text in texts:
        tokens, separators = construct_tokens_separators(
            text,
            ngram_size_left=ngram_size_left,
            ngram_size_right=ngram_size_right)
        bows.append(BagOfWords().fit(tokens))
        ys.append(np.array(separators, dtype=int))
    return bows, ys


_classifiers = [
        LogisticRegression(),
        SGDClassifier(),  # linear SVM
        BernoulliNB(),
        ComplementNB(),
        MultinomialNB(),
        RandomForestClassifier(),
        GradientBoostingClassifier(),
        MLPClassifier(hidden_layer_sizes=(32, 32)),
]

def main(ngram_size_left, ngram_size_right, classifiers=_classifiers):
    logging.info("construct datasets")
    Xs, ys = construct_Xy(
        text_train, text_test,
        ngram_size_left=ngram_size_left, ngram_size_right=ngram_size_right)

    X_train, X_test = Xs[:2]
    y_train, y_true = ys[:2]

    logging.info("align vocabularray")
    # alignement of the vocabularrays (not optimal, one should keep the train vocab)
    common_voc = np.isin(X_test.vocabularray, X_train.vocabularray)
    X_test = BagOfWords(
        bag=X_test.bag[:, common_voc],
        doctionarray=X_test.doctionarray,
        vocabularray=X_test.vocabularray[common_voc])
    common_voc = np.isin(X_train.vocabularray, X_test.vocabularray)
    X_train = BagOfWords(
        bag=X_train.bag[:, common_voc],
        doctionarray=X_train.doctionarray,
        vocabularray=X_train.vocabularray[common_voc])

    logging.info("start learning")

    stringlength = max(len(cl.__class__.__name__) for cl in classifiers)
    scores, precision = [], 5
    for classifier in classifiers:
        duration = dt.now()
        classifier.fit(X_train.bag, y_train)
        y_pred = classifier.predict(X_test.bag)
        # skmet.confusion_matrix(y_true, y_pred)
        string = classifier.__class__.__name__.ljust(stringlength + 1) + ': '
        balanced_accuracy = skmet.balanced_accuracy_score(y_true, y_pred)
        balanced_accuracy = round(balanced_accuracy, precision)
        string += f"bal. acc. {balanced_accuracy}"
        mutual_info = skmet.normalized_mutual_info_score(y_true, y_pred)
        mutual_info = round(mutual_info, precision)
        string += f" | mutual info. {mutual_info}"
        confusion_matrix = skmet.confusion_matrix(y_true, y_pred)
        # sc = skmet.adjusted_mutual_info_score(y_true, y_pred)
        # string += f" | adjusted mutinf. {sc:.5f}"
        logging.info(string)
        duration = dt.now() - duration
        duration = round(duration.total_seconds(), precision)
        score = dict(
            model=classifier.__class__.__name__,
            ngram_size_left=ngram_size_left,
            ngram_size_right=ngram_size_right,
            balanced_accuracy=balanced_accuracy,
            true_positive=confusion_matrix[1][1],
            true_negative=confusion_matrix[0][0],
            false_positive=confusion_matrix[0][1],
            false_negative=confusion_matrix[1][0],
            mutual_info=mutual_info,
            training_duration=duration,
        )
        scores.append(score)
    return scores


if __name__ == '__main__':
    import pandas
    logging.info("start the script")
    scores = []
    for ngram_size_left in range(2, 3):
        for ngram_size_right in range(0, ngram_size_left + 1):
            logging.info(f"sizes left {ngram_size_left} and right {ngram_size_right}")
            scores.extend(main(ngram_size_left, ngram_size_right))
    df = pandas.DataFrame(scores)
    datafile = "data/binary_classifiers.csv"
    df.to_csv(datafile)
    logging.info(f"data saved as {datafile}")
    logging.info("end of script")
